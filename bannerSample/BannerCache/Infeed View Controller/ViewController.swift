//
//  ViewController.swift
//  BannerCahce
//
//  Created by Mohamed Matloub on 4/29/21.
//

import UIKit
import AATKit

enum InfeedType {
	case author(author: String)
	case ad(adView: UIView?)
}


class ViewController: UIViewController {
	@IBOutlet weak private var tableView: UITableView!

	private var bannerCache: AATBannerCache?
	fileprivate var adsFirstIndex = 2
	fileprivate var adsRecursivity = 8

	private var data: [InfeedType] = []

	override func viewDidLoad() {
		super.viewDidLoad()
		tableView.delegate = self
		tableView.dataSource = self
		tableView.rowHeight = UITableView.automaticDimension

		prepareData()

		setupBannerCache()
	}

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        AATSDK.controllerViewDidAppear(controller: self)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        AATSDK.controllerViewWillDisappear()
    }
    
	func prepareData() {
		let arr = authors.compactMap({ InfeedType.author(author: $0) })
		var infeedArr = arr
		for i in stride(from: self.adsFirstIndex, to: arr.count, by: self.adsRecursivity) {
			infeedArr.insert(.ad(adView: nil), at: i)
		}
		self.data = infeedArr
		self.tableView.reloadData()
	}

	func setupBannerCache() {
        let configuration = AATBannerCacheConfiguration(placementName: "placement", size: 3)
        let request = AATBannerRequest(delegate: self)
        request.contentTargetingUrl = ""
        request.targetingInformation = ["key": ["values"]]
        request.setRequestBannerSizes(sizes: Set(arrayLiteral: .banner320x53, .banner300x250))

        configuration.delegate = self
        configuration.requestConfiguration = request
        configuration.bannerRequestDelegate = self
        bannerCache = AATSDK.createBannerCache(configuration: configuration)
        bannerCache?.impressionDelegate = self
	}

	deinit {
		// [ADS] Once you no longer need this instance of AATBannerCache, call the following to clear the memory
		bannerCache?.destroy()
		bannerCache = nil
	}
}

extension ViewController: UITableViewDataSource {
	func numberOfSections(in tableView: UITableView) -> Int {
		return 1
	}

	func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
		return data.count
	}

	func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
		let current = data[indexPath.item]
		switch current {
		case .author(let author):
			let cell = tableView.dequeueReusableCell(withIdentifier: "cell") as? InfeedTableViewCell
			cell?.titleLabel.text = author
			return cell ?? UITableViewCell()

		case .ad(adView: let view):
			// [ADS]  manage presentation of ad view
			let cell = tableView.dequeueReusableCell(withIdentifier: "adCell", for: indexPath) as? AdTableViewCell
			if let adView = view {
				cell?.addAdView(adView)
			} else if let adView = bannerCache?.consume() { // [ADS] consume adView from bannerCache
				cell?.addAdView(adView)
				self.data[indexPath.row] = .ad(adView: adView)
			}

			return cell ?? UITableViewCell()
		}
	}

	func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
		let current = data[indexPath.item]
		switch current {
		case .author:
			return UITableView.automaticDimension
		case .ad:
			return 250
		}
	}
}

extension ViewController: UITableViewDelegate {
	func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
		guard let _ = tableView.cellForRow(at: indexPath) as? InfeedTableViewCell,
		let detailsViewController = self.storyboard?.instantiateViewController(withIdentifier: "DetailsViewController") as? DetailsViewController
		else {return}
		detailsViewController.bannerCache = bannerCache
		navigationController?.pushViewController(detailsViewController, animated: true)
	}
}


extension ViewController: AATBannerCacheDelegate {
	func firstBannerLoaded() {
		tableView.reloadData()
	}
}

extension ViewController: AATBannerRequestDelegate {
    func shouldUseTargeting(for request: AATBannerRequest, network: AATAdNetwork) -> Bool {
        return true
    }
}

extension ViewController: AATImpressionDelegate {
    func didCountImpression(placement: AATKit.AATPlacement?, _ impression: AATKit.AATImpression) {
    }
}
