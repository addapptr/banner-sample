//
//  DetailsViewController.swift
//  BannerCache
//
//  Created by Mohamed Matloub on 5/4/21.
//

import UIKit
import AATKit

class DetailsViewController: UIViewController {
	@IBOutlet weak var stickyBannerView: UIView!
	@IBOutlet private weak var adsHeightConstraint: NSLayoutConstraint!
	@IBOutlet private weak var adsWidthConstraint: NSLayoutConstraint!

	var bannerCache: AATBannerCache?

	override func viewDidLoad() {
		super.viewDidLoad()
		if let view = bannerCache?.consume() {
			displayStickyBanner(bannerView: view)
		}
	}

    override func viewDidAppear(_ animated: Bool) {
        super.viewWillAppear(animated)

        AATSDK.controllerViewDidAppear(controller: self)
    }

	func displayStickyBanner(bannerView: UIView) {
		// place the banner view
		removeSubviewsFor(parentView: stickyBannerView)

		bannerView.removeFromSuperview()
		bannerView.translatesAutoresizingMaskIntoConstraints = false
		self.stickyBannerView.addSubview(bannerView)
		self.stickyBannerView.centerXAnchor.constraint(equalTo: bannerView.centerXAnchor).isActive = true
		self.stickyBannerView.centerYAnchor.constraint(equalTo: bannerView.centerYAnchor).isActive = true
		bannerView.heightAnchor.constraint(equalToConstant: bannerView.frame.height).isActive = true
		bannerView.widthAnchor.constraint(equalToConstant: bannerView.frame.width).isActive = true
		adsHeightConstraint.constant = bannerView.frame.height
		adsWidthConstraint.constant = bannerView.frame.width
	}

	func removeSubviewsFor(parentView: UIView) {
		parentView.subviews.forEach { $0.removeFromSuperview() }
	}
}
