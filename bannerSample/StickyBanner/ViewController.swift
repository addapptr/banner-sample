//
//  ViewController.swift
//  StickyBanner
//
//  Created by Mohamed Matloub on 4/29/21.
//

import UIKit
import AATKit

class ViewController: UIViewController {
	@IBOutlet weak var stickyBannerView: UIView!
	@IBOutlet private weak var adsHeightConstraint: NSLayoutConstraint!
	
	// Sticky Placement
	var stickyBannerPlacement: AATStickyBannerPlacement?

	override func viewDidLoad() {
		super.viewDidLoad()

        stickyBannerPlacement = AATSDK.createStickyBannerPlacement(name: "StickyBannerPlacement", size: .banner320x53)
        stickyBannerPlacement?.delegate = self
        stickyBannerPlacement?.statisticsDelegate = self
	}

	override func viewDidAppear(_ animated: Bool) {
		super.viewDidAppear(animated)
		// [ADS] - Refresh Ads for placement
		// [ADS] you may set keyword targeting here for placement
        AATSDK.controllerViewDidAppear(controller: self)

        // Get the banner view
        if let bannerAdView = stickyBannerPlacement?.getPlacementView() {
            displayStickyBanner(bannerView: bannerAdView)
        }
        DispatchQueue.main.asyncAfter(deadline: .now() + 2, execute: {
            self.stickyBannerPlacement?.startAutoReload()
        })
	}

	override func viewWillDisappear(_ animated: Bool) {
		super.viewWillDisappear(animated)
		// [ADS] - stop auto reloading for placement "ProgramPage"
        stickyBannerPlacement?.stopAutoReload()
        AATSDK.controllerViewWillDisappear()
	}

	func displayStickyBanner(bannerView: UIView) {
		// place the banner view
		removeSubviewsFor(parentView: stickyBannerView)

		bannerView.removeFromSuperview()
		bannerView.translatesAutoresizingMaskIntoConstraints = false
		self.stickyBannerView.addSubview(bannerView)
		self.stickyBannerView.centerXAnchor.constraint(equalTo: bannerView.centerXAnchor).isActive = true
		self.stickyBannerView.centerYAnchor.constraint(equalTo: bannerView.centerYAnchor).isActive = true
		bannerView.heightAnchor.constraint(equalToConstant: bannerView.frame.height).isActive = true
		bannerView.widthAnchor.constraint(equalToConstant: bannerView.frame.width).isActive = true
		adsHeightConstraint.constant = bannerView.frame.height
	}

	func removeSubviewsFor(parentView: UIView) {
		parentView.subviews.forEach { $0.removeFromSuperview() }
	}
}
extension ViewController: AATStickyBannerPlacementDelegate {
    func aatPauseForAd(placement: AATPlacement) {
    }

    func aatAdCurrentlyDisplayed(placement: AATPlacement) {
        print(#function)
    }
    
    func aatResumeAfterAd(placement: AATPlacement) {
        print(#function)
    }
    
    func aatHaveAd(placement: AATPlacement) {
        print(#function)
    }
    
    func aatNoAd(placement: AATPlacement) {
        print(#function)
    }
}

extension ViewController: AATStatisticsDelegate {
    func AATKitCountedNetworkImpression(placement: AATPlacement?, for network: AATKit.AATAdNetwork) {
    }

    func AATKitCountedMediationCycle(placement: AATPlacement?) {
        print(#function)
    }
    
    func AATKitCountedAdSpace(placement: AATPlacement?) {
        print(#function)
    }
    
    func AATKitCountedRequest(placement: AATPlacement?, for network: AATAdNetwork) {
        print(#function)
    }
    
    func AATKitCountedResponse(placement: AATPlacement?, for network: AATAdNetwork) {
        print(#function)
    }
    
    func AATKitCountedImpression(placement: AATPlacement?, for network: AATAdNetwork) {
        print(#function)
    }
    
    func AATKitCountedVImpression(placement: AATPlacement?, for network: AATAdNetwork) {
        print(#function)
    }
    
    func AATKitCountedClick(placement: AATPlacement?, for network: AATAdNetwork) {
        print(#function)
    }
    
    func AATKitCountedDirectDealImpression(placement: AATPlacement?, for network: AATAdNetwork) {
        print(#function)
    }
}
