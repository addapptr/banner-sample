//
//  NotificationsNames.swift
//  StickyBanner
//
//  Created by Mohamed Matloub on 4/30/21.
//

import Foundation

enum NotificationsNames: String {
	case aatKitHaveAd
	case aatKitNoAds
}
