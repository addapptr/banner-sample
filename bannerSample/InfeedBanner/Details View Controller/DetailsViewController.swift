//
//  DetailsViewController.swift
//  InfeedBanner
//
//  Created by Mohamed Matloub on 5/4/21.
//

import UIKit
import AATKit

class DetailsViewController: UIViewController {
	@IBOutlet weak var stickyBannerView: UIView!
	@IBOutlet private weak var adsHeightConstraint: NSLayoutConstraint!
	@IBOutlet private weak var adsWidthConstraint: NSLayoutConstraint!

	var inFeedBannerPlacement: AATInfeedBannerPlacement?
	
	override func viewDidLoad() {
		super.viewDidLoad()
        inFeedBannerPlacement = AATSDK.createInfeedBannerPlacement(name: "placement", configuration: AATBannerConfiguration())

        let request = AATBannerRequest(delegate: self)
        request.setRequestBannerSizes(sizes: [.multipleSizes, .banner320x53])
        inFeedBannerPlacement?.requestAd(request: request, completion: { [weak self]  (view, error) in
            guard let view = view else {
                return
            }
            self?.displayStickyBanner(bannerView: view)
        })
	}

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        AATSDK.controllerViewDidAppear(controller: self)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        AATSDK.controllerViewWillDisappear()
    }
    
	func displayStickyBanner(bannerView: UIView) {
		// place the banner view
		removeSubviewsFor(parentView: stickyBannerView)

		bannerView.removeFromSuperview()
		bannerView.translatesAutoresizingMaskIntoConstraints = false
		self.stickyBannerView.addSubview(bannerView)
		self.stickyBannerView.centerXAnchor.constraint(equalTo: bannerView.centerXAnchor).isActive = true
		self.stickyBannerView.centerYAnchor.constraint(equalTo: bannerView.centerYAnchor).isActive = true
		bannerView.heightAnchor.constraint(equalToConstant: bannerView.frame.height).isActive = true
		bannerView.widthAnchor.constraint(equalToConstant: bannerView.frame.width).isActive = true
		adsHeightConstraint.constant = bannerView.frame.height
		adsWidthConstraint.constant = bannerView.frame.width
	}

	func removeSubviewsFor(parentView: UIView) {
		parentView.subviews.forEach { $0.removeFromSuperview() }
	}
}

extension DetailsViewController: AATBannerRequestDelegate {
    func shouldUseTargeting(for request: AATBannerRequest, network: AATAdNetwork) -> Bool {
        return true
    }
}

extension DetailsViewController: AATStatisticsDelegate {
    func AATKitCountedNetworkImpression(placement: AATPlacement?, for network: AATKit.AATAdNetwork) {
    }

    func AATKitCountedMediationCycle(placement: AATPlacement?) {
        print("\(#function)")
    }
    
    func AATKitCountedAdSpace(placement: AATPlacement?) {
        print("\(#function)")
    }
    
    func AATKitCountedRequest(placement: AATPlacement?, for network: AATAdNetwork) {
        print("\(#function) -> \(network)")
    }
    
    func AATKitCountedResponse(placement: AATPlacement?, for network: AATAdNetwork) {
        print("\(#function) -> \(network)")
    }
    
    func AATKitCountedImpression(placement: AATPlacement?, for network: AATAdNetwork) {
        print("\(#function) -> \(network)")
    }
    
    func AATKitCountedVImpression(placement: AATPlacement?, for network: AATAdNetwork) {
        print("\(#function) -> \(network)")
    }
    
    func AATKitCountedClick(placement: AATPlacement?, for network: AATAdNetwork) {
        print("\(#function) -> \(network)")
    }
    
    func AATKitCountedDirectDealImpression(placement: AATPlacement?, for network: AATAdNetwork) {
        print("\(#function) -> \(network)")
    }
}

