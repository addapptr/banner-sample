//
//  ViewController.swift
//  InfeedBanner
//
//  Created by Mohamed Matloub on 4/29/21.
//

import UIKit
import AATKit

enum InfeedType {
	case author(author: String)
	case ad(adView: UIView?)
}

class ViewController: UIViewController {
	@IBOutlet weak private var tableView: UITableView!

	// MARK: [ADS] - Ads infeed configuration
	fileprivate var adsFirstIndex = 2
	fileprivate var adsRecursivity = 8

	private var data: [InfeedType] = []

	var inFeedBannerPlacement: AATInfeedBannerPlacement?
    
	override func viewDidLoad() {
		super.viewDidLoad()
		tableView.delegate = self
		tableView.dataSource = self
		tableView.rowHeight = UITableView.automaticDimension
        let configuration = AATBannerConfiguration()

        inFeedBannerPlacement = AATSDK.createInfeedBannerPlacement(name: "placement",
                                                                          configuration: configuration)
        inFeedBannerPlacement?.statisticsDelegate = self
        inFeedBannerPlacement?.impressionDelegate = self
        
		prepareData()
	}

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)

        AATSDK.controllerViewDidAppear(controller: self)
        requestBannerAd()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        AATSDK.controllerViewWillDisappear()
    }
    
	func prepareData() {
		let arr = authors.compactMap({ InfeedType.author(author: $0) })
		var infeeedArr = arr
		for index in stride(from: self.adsFirstIndex, to: arr.count, by: self.adsRecursivity) {
			infeeedArr.insert(.ad(adView: nil), at: index)
		}
		self.data = infeeedArr
		self.tableView.reloadData()
	}

	func requestBannerAd() {
		let arr = self.data
        for index in stride(from: self.adsFirstIndex, to: arr.count, by: self.adsRecursivity) {
            //[ADS] create the request responsible for the AD, you can specify the banner size, targeting keywords if needed and contentTargetingURL
            let request = AATBannerRequest(delegate: self)
            request.setRequestBannerSizes(sizes: [.banner300x250, .banner320x53])
            // each request can have its own targeting keywords
            request.targetingInformation = ["rowNumber": [ "\(index)"]]
            inFeedBannerPlacement?.requestAd(request: request, completion: { [weak self]  (view, error) in
                guard let view = view else {
                    return
                }
                self?.data[index] = .ad(adView: view)
                self?.tableView.reloadData()
            })
        }
    }
}

extension ViewController: UITableViewDataSource {
	func numberOfSections(in tableView: UITableView) -> Int {
		return 1
	}

	func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
		return data.count
	}

	func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
		let current = data[indexPath.item]
		switch current {
		case .author(let author):
			let cell = tableView.dequeueReusableCell(withIdentifier: "cell") as? InfeedTableViewCell
			cell?.titleLabel.text = author
			return cell ?? UITableViewCell()

		case .ad(adView: let view):
			// [ADS]  manage presentation of ad view
			let cell = tableView.dequeueReusableCell(withIdentifier: "adCell", for: indexPath) as? AdTableViewCell
			if let adView = view {
				cell?.addAdView(adView)
			} 

			return cell ?? UITableViewCell()
		}
	}

	func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
		let current = data[indexPath.item]
		switch current {
		case .author:
			return UITableView.automaticDimension
		case .ad:
			return UITableView.automaticDimension
		}
	}
}

extension ViewController: UITableViewDelegate {
	func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
		guard let _ = tableView.cellForRow(at: indexPath) as? InfeedTableViewCell,
			  let detailsViewController = self.storyboard?.instantiateViewController(withIdentifier: "DetailsViewController") as? DetailsViewController
		else {return}
		navigationController?.pushViewController(detailsViewController, animated: true)
	}
}

extension ViewController: AATStatisticsDelegate {
    func AATKitCountedNetworkImpression(placement: AATPlacement?, for network: AATKit.AATAdNetwork) {
    }

    func AATKitCountedMediationCycle(placement: AATPlacement?) {
        print("\(#function)")
    }
    
    func AATKitCountedAdSpace(placement: AATPlacement?) {
        print("\(#function)")
    }
    
    func AATKitCountedRequest(placement: AATPlacement?, for network: AATAdNetwork) {
        print("\(#function) -> \(network)")
    }
    
    func AATKitCountedResponse(placement: AATPlacement?, for network: AATAdNetwork) {
        print("\(#function) -> \(network)")
    }
    
    func AATKitCountedImpression(placement: AATPlacement?, for network: AATAdNetwork) {
        print("\(#function) -> \(network)")
    }
    
    func AATKitCountedVImpression(placement: AATPlacement?, for network: AATAdNetwork) {
        print("\(#function) -> \(network)")
    }
    
    func AATKitCountedClick(placement: AATPlacement?, for network: AATAdNetwork) {
        print("\(#function) -> \(network)")
    }
    
    func AATKitCountedDirectDealImpression(placement: AATPlacement?, for network: AATAdNetwork) {
        print("\(#function) -> \(network)")
    }
}

extension ViewController: AATImpressionDelegate {
    func didCountImpression(placement: AATPlacement?, _ impression: AATImpression) {
        print("\(impression)")
    }
}

extension ViewController: AATBannerRequestDelegate {
    func shouldUseTargeting(for request: AATBannerRequest, network: AATAdNetwork) -> Bool {
        return true
    }
}
